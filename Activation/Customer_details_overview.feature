@OD @OD @JIRA-BBB-40
Feature: Customer details overview
    "**search customer in operation dashboard using CIF ID and view customer details overview**"

  @Positive
  Scenario: Verify that all the information displayed
    | Company Name| Company Registration Number | Customer Segment | Customer Status | Phone Number| Email Address |
    Established (founded year) will display only Year.
    Given customer details tab will be selected as default in overview screen
    When verify customer details over screen
    Then all the information should be displayed correctly
      """
      Company Name
      Company Registration Number
      Customer Segment
      Customer Status
      Phone Number
      Email Address
      """
    And if there are multiple numbers and emails, all numbers and emails should be displayed
    And customer status will always be active in this screen
    When verify customer details screen
    Then all the information should be displayed correctly
      """
      CIF ID
      Company Name
      Fax Number
      Company Registration Number
      Country of Incorporation
      Registration Date
      Established
      """
    And CIF ID will be enterprise ID only
    And date format should be DD:MM:YY
    And established year format should be YY
    And country of incorporation code should be displayed
      """
      For instance, 
      Myanmar => MMR
      USA => United State of America
      """
    When check address information
    Then all the information should be displayed correctly
      """
      Address Type
      Country
      State
      Postal Code
      City
      Address
      Primary Address
      """
    And address type should be Home address or Mailing address or Office address or DEFAULT
    And if there are more than one address, all addresses should be displayed

  @Positive
  Scenario: Verify that all the tabs are clickable
    Given customer details tab will be selected as default in overview screen
    When click to the "limitations tab" from the "customer details tab"
    Then display "the limitations screen"
    When click to the "authorizations tab" from the "limitations tab"
    Then display "authorization screen"
    When click to the "entitlement tab" from the "limitations tab"
    Then display "entitlement screen"
    When click to the "device management tab" from the "entitlement tab"
    Then display "device management screen"

  @Positive
  Scenario: Verify successful logout functionality
    Given customer details tab will be selected as default in overview screen
    When click the "profile button"
    And click the "logout button"
    Then redirect to "customer management screen"

  @Positive
  Scenario: Verify clicking back button on customer detail overview screen
    Given customer details tab will be selected as default in overview screen
    When click the "back button"
    Then redirect to "customer management screen"
