@OD @OD
Feature: Entity Activation
    "**operation dashboard admin account login to the portal**"

  @Positive
  Scenario Outline: Search customer using valid ECIF/related PCIFS
    Given logged to operation dashboard home screen
    When click the "customer management menu"
    Then display customer management screen
    When enter CIF ID "<cifid>"
    And click the "search button"
    Then display enterprise customer details overview in list
    When click the "customer"
    Then user able to see "the customer details screen"

    Examples:
      | cifid |
      | 10662463 |
      | 10662451 |

  @Negative
  Scenario Outline: Search customer using invalid ECIF/related PCIFs
    digit only accept in party ID input field
    - no space between digits
    - no special characters 
    - no characters
    Given logged to operation dashboard home screen
    When click the "customer management menu"
    And enter invalid CIF ID "<invalidcifid>"
    And click the "search button"
    Then user able to see "<error_message>"
    And the result table should be hidden

    Examples:
      | invalidcifid | error_message |
      | !@#456 | Invalid CIF ID. Please try again. |
      | 12345678901234567890123456789012345678901234567890 | Invalid CIF ID. Please try again. |
      | 19898989 | Invalid CIF ID. Please try again. |
      | 10662  463 | Invalid CIF ID. Please try again. |
      | notpassword | Invalid CIF ID. Please try again. |
      | 1066245 | Invalid CIF ID. Please try again. |
      | 10669090 | Invalid CIF ID. Please try again. |
      | 10102237 | Invalid CIF ID. Please try again. |
      | 10120349 | Invalid CIF ID. Please try again. |
      | 00904076 | Invalid CIF ID. Please try again. |
      | 10077872 | Invalid CIF ID. Please try again. |

  @Positive
  Scenario Outline: Search customer using valid registration number
    Given logged to operation dashboard home screen
    When click the "customer management menu"
    Then display customer management screen
    When enter company registration number "<reg_number>"
    And click the "search button"
    Then display customer details overview in list
    When verify the displayed information
    Then all the information should be displayed correctly
      """
      company name
      company registration number 
      customer segment
      customer status
      """
    And click the "customer"
    Then user able to see "customer details screen"

    Examples:
      | reg_number |
      | 123456 |

  @Negative
  Scenario Outline: Search customer using invalid registration number
    Given logged to operation dashboard home screen
    When click the "customer management menu"
    Then display customer management screen
    When enter invalid company registration number "<registration_number>"
    And click the "search button"
    Then user able to see "<error_message>"

    Examples:
      | registration_number | error_message |
      | 12345678 | Invalid registration number. Please try again. |
      | 123456789012345678901234567890 | Invalid registration number. Please try again. |
      | ~!`@#$%^&*()_ | Invalid registration number. Please try again. |
      | alphabets as numbers | Invalid registration number. Please try again. |
      | 123 45 44 | Invalid registration number. Please try again. |
      | 123456780 | Invalid registration number. Please try again. |

  @Negative
  Scenario Outline: Search customer by leaving CIF ID and register number field blank
    Given logged to operation dashboard home screen
    When click the "customer management menu"
    And left the CIF ID "<cif_id>" input field blank
    And left the registration number "<registration_number>" input field blank
    And click the "search button"
    Then user able to see "that the search button remains disable"

    Examples:
      | cif_id | registration_number |
      |  |  |

  @Positive
  Scenario Outline: Verify that all the information displayed in the result list
    Company Name 

    Company Registration Number 

    Customer Segment 

    Customer Status
    Given logged to operation dashboard home screen
    When click the "customer management menu"
    Then display customer management screen
    When enter CIF ID "<cif_id>"
    And click the "search button"
    Then display customer details overview in list
    When verify the displayed information
    Then all the information should be displayed correctly
      """
      company name
      company registration number
      customer segment
      customer status 
      """

    Examples:
      | cif_id |
      | 12312323 |

  @Positive
  Scenario: Verify clicking on browser back from the entity activation screen
    Given logged to operation dashboard home screen
    When click the "customer management menu"
    And click the "back from browser"
    Then user able to see "the previous screen"
