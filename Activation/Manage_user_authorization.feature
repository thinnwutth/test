@OD @OD @JIRA-BBB-35
Feature: Manage user authorization
    "**Relationship users' permission and authorization on enterprise account management in operation dashboard**"

  Scenario: Disable authorizations access right
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When select a user from the list and click manage button

  Scenario: Enable full right for customer level and account level authorizations
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When select a user from the list and click manage button
    Then display manage authorization screen and access rights are off in default stage

  Scenario: Enable no right for customer level and account level authorizations
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When select a user from the list and click manage button
    Then display manage authorization screen and access rights are off in default stage

  Scenario: Enable read only right for customer level and account level authorizations
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When select a user from the list and click manage button
    Then display manage authorization screen and access rights are off in default stage

  Scenario: Enable full right for user level and no right for account level authorizations
    "**test case cover for multiple product(bank accounts) owners as well**"
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When select a user from the list and click manage button
    Then display manage authorization screen and access rights are off in default stage

  Scenario: Enable full right for user level and read only right for account level authorizations
    "**test case cover for multiple product(bank accounts) owners as well**"
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When select a user from the list and click manage button
    Then display manage authorization screen and access rights are off in default stage

  Scenario: Enable no right for user level and full right for account level authorizations
    "**test case cover for multiple product(bank accounts) owners as well**"
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When select a user from the list and click manage button
    Then display manage authorization screen and access rights are off in default stage

  Scenario: Enable read only right for user level and full right in account level authorizations
    "**test case cover for multiple product(bank accounts) owners as well**"
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When select a user from the list and click manage button
    Then display manage authorization screen and access rights are off in default stage

  Scenario: Display data verification for manage authorization screen
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    And select a user from the list and click manage button
    Then display manage authorization screen and access rights are off in default stage
      """
      Customer level authorization session
      Account level authorization session
      """
    When verify the displayed information
    When verify customer level authorization
    Then all the information in customer level authorization display correctly
      """
      Access Right Dropdown:  Full right, Read only right, No right
      """
    When verify account level authorization
    Then all the information in account level authorization display correctly
      """
      Product  (Account Number)
      Product Group  (Account Type)
      Access Right :  Full right, Read only right, No right.
      """

  Scenario: Display data verification for authorization tab
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When verify the displayed information
    Then all the information should be displayed correctly
      """
      PCIF ID
      Name
      Relationship Type
      NRC Number
      Customer Segment
      Customer Status
      Manage (clickable hyper link)
      """

  Scenario: Verify logout from authorization tab
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When click the "profile button"

  Scenario: Verify back button from authorization tab
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When click the "back button"
    Then redirect to "customer management screen"

  Scenario: Verify back button from authorization tab (web back button)
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When click the "web back button"
    Then redirect to "customer management screen"

  Scenario: Verify clickable buttons on manage authorization screen
    Given customer details tab will be selected as default in overview screen
    When click the "authorization tab"
    Then the authorization page display the list of relationship users
    When select a user from the list and click manage button
    Then display manage authorization screen and access rights are off in default stage
    And select drop down from customer level authorization
    And verify the drop down list
    Then display drop down list
      """
      full right
      read only right
      no right

      """
    And verify the drop down list
    Then display drop down list
      """
      full right
      read only right
      no right

      """
