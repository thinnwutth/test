package com.example;

public class Actionwords {

    public void navigateTheProtalUrl(String protal) {

    }

    public void theUserEnterUserNameUsernameAndPasswordPassword(String username, String password) {

    }

    public void clickTheAnybutton(String anybutton) {

    }

    public void userAbleToSeeAnyScreen(String anyScreen, String freeText) {

    }

    public void enterInvalidUserNameUsernameAndPasswordPassword(String username, String password) {

    }

    public void ableToSeeErrorMessageForInvalidUsername() {

    }

    public void theUserEnterUserNameP1AndInvalidPasswordP2(String p1, String p2) {

    }

    public void ableToSeeErrorMessageForInvalidPassword() {

    }

    public void enterInvalidUserNameUsernameAndorInvalidPasswordInvalidPasswordForTheFirstTime(String username, String invalidPassword) {

    }

    public void ableToSeeErrorMessageForInvalidCredentials() {

    }

    public void enterInvalidUserNameUsernameAndorInvalidPasswordInvalidPasswordForTheSecondTime(String username, String invalidPassword) {

    }

    public void enterInvalidUserNameUsernameAndorInvalidPasswordPasswordForTheThirdTime(String username, String password) {

    }

    public void enterInvalidUserNameUsernameAndorInvalidPasswordInvalidPasswordForTheFourthTime(String username, String invalidPassword) {

    }

    public void enterInvalidUserNameUsernameAndorInvalidPasswordPasswordForTheFifthTime(String username, String password) {

    }

    public void enterInvalidUserNameInvalidUsernameAndorInvalidPasswordInvalidPasswordForTheSixthTime(String invalidUsername, String invalidPassword) {

    }

    public void ableToSeeErrorMessageSomethingsWrongWithYourAccountPleaseContactOurCustomerCareCenter() {

    }

    public void enterCIFIDId(String id) {

    }

    public void clickToTheWebPage1stFromTheWebPage2nd(String webPage1st, String webPage2nd) {

    }

    public void enterInvalidCIFIDCIF(String cIF) {

    }

    public void customerDetailsTabWillBeSelectedAsDefaultInOverviewScreen() {

    }

    public void theAuthorizationPageDisplayTheListOfRelationshipUsers() {

    }

    public void selectAUserFromTheListAndClickManageButton() {

    }

    public void enableOnlinePortalBySwitchingTheToggleOn() {

    }

    public void disableOnlineBySwitchingTheToggleOff() {

    }

    public void disableMobileBySwitchingTheToggleOff() {

    }

    public void selectDropDownFromCustomerLevelAuthorization() {

    }

    public void selectFullRightFromTheDropDownList() {

    }

    public void selectDropDownFromTheCustomerLevelAuthorization() {

    }

    public void enableMobileBySwitchingTheToggleOn() {

    }

    public void selectNoRightFromTheDropDownList() {

    }

    public void selectDropDownFromAccountLevelAuthorization() {

    }

    public void selectDropDownFromTheAccount() {

    }

    public void selectReadOnlyRightFromTheDropDownList() {

    }

    public void displayManageAuthorizationScreenAndAccessRightsAreOffInDefaultStage(String freeText) {

    }

    public void displayManageAuthorizationsPopUpWindowAndToggleAreOn() {

    }

    public void leftTheCIFIDCifIdInputFieldBlank(String cifId) {

    }

    public void displayCustomerManagementScreen() {

    }

    public void loggedToOperationDashboardHomeScreen() {

    }

    public void displayCustomerDetailsOverviewInList() {

    }

    public void leftTheRegistrationNumberRegistrationNumberInputFieldBlank(String registrationNumber) {

    }

    public void enterCompanyRegistrationNumberP1(String p1) {

    }

    public void enterInvalidCompanyRegistrationNumberP1(String p1) {

    }

    public void enterValidCifIdCIF(String cIF) {

    }

    public void enterInvalidCifIdP1AndInvalidRegistrationNumberP2(String p1, String p2) {

    }

    public void verifyCustomerDetailsOverScreen(String freeText) {

    }

    public void verifyCustomerDetailsScreen(String freeText) {

    }

    public void redirectToRedirectScreen(String redirectScreen) {

    }

    public void displayP1(String p1) {

    }

    public void allTheInformationShouldBeDisplayedCorrectly(String freeText) {

    }

    public void ifThereAreMultipleNumbersAndEmailsAllNumbersAndEmailsShouldBeDisplayed() {

    }

    public void dateFormatShouldBeDDMMYY() {

    }

    public void establishedYearFormatShouldBeYY() {

    }

    public void countryOfIncorporationCodeShouldBeDisplayed(String freeText) {

    }

    public void cIFIDWillBeEnterpriseIDOnly() {

    }

    public void customerStatusWillAlwaysBeActiveInThisScreen() {

    }

    public void displayEnterpriseCustomerDetailsOverviewInList() {

    }

    public void checkAddressInformation() {

    }

    public void theActionPerformedShouldNotBeSaved() {

    }

    public void addressTypeShouldBeHomeAddressOrMailingAddressOrOfficeAddressOrDEFAULT() {

    }

    public void accessRightForOnlineWillBeFullRightInDefault() {

    }

    public void accessRightForMobileWillBeFullRightInDefault() {

    }

    public void ifThereAreMoreThanOneAddressAllAddressesShouldBeDisplayed() {

    }

    public void theResultTableShouldBeHidden() {

    }

    public void allTheInformationInCustomerDetailsDisplayCorrectly(String freeText) {

    }

    public void verifyCustomerDetails() {

    }

    public void verifyCustomerLevelAuthorization() {

    }

    public void allTheInformationInCustomerLevelAuthorizationDisplayCorrectly(String freeText) {

    }

    public void verifyAccountLevelAuthorization() {

    }

    public void allTheInformationInAccountLevelAuthorizationDisplayCorrectly(String freeText) {

    }

    public void verifyTheDropDownList(String freeText) {

    }

    public void displayDropDownList(String freeText) {

    }

    public void alreadyLoggedInTheP1(String p1) {

    }

    public void clickOutsideThePopUpWindow() {

    }

    public void popUpWindowShouldNotClose() {

    }

    public void verifyTheDisplayedInformation() {

    }

    public void enterValidRegistrationNumberRegistrationNumber(String registrationNumber) {

    }

    public void enterInvalidRegistrationNumberRegistrationNumber(String registrationNumber) {

    }

    public void selectAnAccountUnderAccountLevelAuthorization() {

    }

    public void scanTheBarcode() {

    }

    public void enterOTPOtp(String otp) {

    }

    public void enterDeviceNameDevicename(String devicename) {

    }

    public void enterOTPOtpFromAuthenticator(String otp) {

    }

    public void enterOTPOtpFromSms(String otp) {

    }

    public void customersPhoneNumberHasAlreadyMarkedAsUserForContact() {

    }

    public void customersEmailHasAlreadyMarkedAsUserForContact() {

    }

    public void enterOTPOtpFromEmail(String otp) {

    }

    public void userShouldBeAbleToReceiveTheEmail(String freeText) {

    }

    public void enterUsernameP1(String p1) {

    }

    public void enterNationalIDP1(String p1) {

    }

    public void enterMobileNumberP1(String p1) {

    }
}