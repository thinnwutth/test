package com.example;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;

public class StepDefinitions {
    public Actionwords actionwords = new Actionwords();

    @Given("navigate the {string} url")
    public void navigateTheProtalUrl(String protal) {
        actionwords.navigateTheProtalUrl(protal);
    }

    @When("the user enter user name {string} and password {string}")
    public void theUserEnterUserNameUsernameAndPasswordPassword(String username, String password) {
        actionwords.theUserEnterUserNameUsernameAndPasswordPassword(username, password);
    }

    @When("click the {string}")
    public void clickTheAnybutton(String anybutton) {
        actionwords.clickTheAnybutton(anybutton);
    }

    @Then("user able to see {string}")
    public void userAbleToSeeAnyScreen(String anyScreen, String freeText) {
        actionwords.userAbleToSeeAnyScreen(anyScreen, freeText);
    }

    @When("enter invalid user name {string} and password {string}")
    public void enterInvalidUserNameUsernameAndPasswordPassword(String username, String password) {
        actionwords.enterInvalidUserNameUsernameAndPasswordPassword(username, password);
    }

    @Then("able to see error message for invalid username")
    public void ableToSeeErrorMessageForInvalidUsername() {
        actionwords.ableToSeeErrorMessageForInvalidUsername();
    }

    @When("the user enter user name {string} and invalid password {string}")
    public void theUserEnterUserNameP1AndInvalidPasswordP2(String p1, String p2) {
        actionwords.theUserEnterUserNameP1AndInvalidPasswordP2(p1, p2);
    }

    @Then("able to see error message for invalid password")
    public void ableToSeeErrorMessageForInvalidPassword() {
        actionwords.ableToSeeErrorMessageForInvalidPassword();
    }

    @Given("enter invalid user name {string} and/or invalid password {string} for the first time")
    public void enterInvalidUserNameUsernameAndorInvalidPasswordInvalidPasswordForTheFirstTime(String username, String invalidPassword) {
        actionwords.enterInvalidUserNameUsernameAndorInvalidPasswordInvalidPasswordForTheFirstTime(username, invalidPassword);
    }

    @Given("able to see error message for invalid credentials")
    public void ableToSeeErrorMessageForInvalidCredentials() {
        actionwords.ableToSeeErrorMessageForInvalidCredentials();
    }

    @Given("enter invalid user name {string} and/or invalid password {string} for the second time")
    public void enterInvalidUserNameUsernameAndorInvalidPasswordInvalidPasswordForTheSecondTime(String username, String invalidPassword) {
        actionwords.enterInvalidUserNameUsernameAndorInvalidPasswordInvalidPasswordForTheSecondTime(username, invalidPassword);
    }

    @Given("enter invalid user name {string} and/or invalid password {string} for the third time")
    public void enterInvalidUserNameUsernameAndorInvalidPasswordPasswordForTheThirdTime(String username, String password) {
        actionwords.enterInvalidUserNameUsernameAndorInvalidPasswordPasswordForTheThirdTime(username, password);
    }

    @Given("enter invalid user name {string} and/or invalid password {string} for the fourth time")
    public void enterInvalidUserNameUsernameAndorInvalidPasswordInvalidPasswordForTheFourthTime(String username, String invalidPassword) {
        actionwords.enterInvalidUserNameUsernameAndorInvalidPasswordInvalidPasswordForTheFourthTime(username, invalidPassword);
    }

    @Given("enter invalid user name {string} and/or invalid password {string} for the fifth time")
    public void enterInvalidUserNameUsernameAndorInvalidPasswordPasswordForTheFifthTime(String username, String password) {
        actionwords.enterInvalidUserNameUsernameAndorInvalidPasswordPasswordForTheFifthTime(username, password);
    }

    @Given("enter invalid user name {string} and/or invalid password {string} for the sixth time")
    public void enterInvalidUserNameInvalidUsernameAndorInvalidPasswordInvalidPasswordForTheSixthTime(String invalidUsername, String invalidPassword) {
        actionwords.enterInvalidUserNameInvalidUsernameAndorInvalidPasswordInvalidPasswordForTheSixthTime(invalidUsername, invalidPassword);
    }

    @Then("able to see error message \\(Something’s wrong with your account\\. Please contact our customer care center\\.\\)")
    public void ableToSeeErrorMessageSomethingsWrongWithYourAccountPleaseContactOurCustomerCareCenter() {
        actionwords.ableToSeeErrorMessageSomethingsWrongWithYourAccountPleaseContactOurCustomerCareCenter();
    }

    @When("enter CIF ID {string}")
    public void enterCIFIDId(String id) {
        actionwords.enterCIFIDId(id);
    }

    @When("click to the {string} from the {string}")
    public void clickToTheWebPage1stFromTheWebPage2nd(String webPage1st, String webPage2nd) {
        actionwords.clickToTheWebPage1stFromTheWebPage2nd(webPage1st, webPage2nd);
    }

    @When("enter invalid CIF ID {string}")
    public void enterInvalidCIFIDCIF(String cIF) {
        actionwords.enterInvalidCIFIDCIF(cIF);
    }

    @Given("customer details tab will be selected as default in overview screen")
    public void customerDetailsTabWillBeSelectedAsDefaultInOverviewScreen() {
        actionwords.customerDetailsTabWillBeSelectedAsDefaultInOverviewScreen();
    }

    @Then("the authorization page display the list of relationship users")
    public void theAuthorizationPageDisplayTheListOfRelationshipUsers() {
        actionwords.theAuthorizationPageDisplayTheListOfRelationshipUsers();
    }

    @When("select a user from the list and click manage button")
    public void selectAUserFromTheListAndClickManageButton() {
        actionwords.selectAUserFromTheListAndClickManageButton();
    }


    @Then("select drop down from customer level authorization")
    public void selectDropDownFromCustomerLevelAuthorization() {
        actionwords.selectDropDownFromCustomerLevelAuthorization();
    }

    @Then("display manage authorization screen and access rights are off in default stage")
    public void displayManageAuthorizationScreenAndAccessRightsAreOffInDefaultStage(String freeText) {
        actionwords.displayManageAuthorizationScreenAndAccessRightsAreOffInDefaultStage(freeText);
    }

    @When("left the CIF ID {string} input field blank")
    public void leftTheCIFIDCifIdInputFieldBlank(String cifId) {
        actionwords.leftTheCIFIDCifIdInputFieldBlank(cifId);
    }

    @Then("display customer management screen")
    public void displayCustomerManagementScreen() {
        actionwords.displayCustomerManagementScreen();
    }

    @Given("logged to operation dashboard home screen")
    public void loggedToOperationDashboardHomeScreen() {
        actionwords.loggedToOperationDashboardHomeScreen();
    }

    @Then("display customer details overview in list")
    public void displayCustomerDetailsOverviewInList() {
        actionwords.displayCustomerDetailsOverviewInList();
    }

    @When("left the registration number {string} input field blank")
    public void leftTheRegistrationNumberRegistrationNumberInputFieldBlank(String registrationNumber) {
        actionwords.leftTheRegistrationNumberRegistrationNumberInputFieldBlank(registrationNumber);
    }

    @When("enter company registration number {string}")
    public void enterCompanyRegistrationNumberP1(String p1) {
        actionwords.enterCompanyRegistrationNumberP1(p1);
    }

    @When("enter invalid company registration number {string}")
    public void enterInvalidCompanyRegistrationNumberP1(String p1) {
        actionwords.enterInvalidCompanyRegistrationNumberP1(p1);
    }

    @When("verify customer details over screen")
    public void verifyCustomerDetailsOverScreen(String freeText) {
        actionwords.verifyCustomerDetailsOverScreen(freeText);
    }

    @When("verify customer details screen")
    public void verifyCustomerDetailsScreen(String freeText) {
        actionwords.verifyCustomerDetailsScreen(freeText);
    }

    @Then("redirect to {string}")
    public void redirectToRedirectScreen(String redirectScreen) {
        actionwords.redirectToRedirectScreen(redirectScreen);
    }

    @Then("display {string}")
    public void displayP1(String p1) {
        actionwords.displayP1(p1);
    }

    @Then("all the information should be displayed correctly")
    public void allTheInformationShouldBeDisplayedCorrectly(String freeText) {
        actionwords.allTheInformationShouldBeDisplayedCorrectly(freeText);
    }

    @Then("if there are multiple numbers and emails, all numbers and emails should be displayed")
    public void ifThereAreMultipleNumbersAndEmailsAllNumbersAndEmailsShouldBeDisplayed() {
        actionwords.ifThereAreMultipleNumbersAndEmailsAllNumbersAndEmailsShouldBeDisplayed();
    }

    @Then("date format should be DD:MM:YY")
    public void dateFormatShouldBeDDMMYY() {
        actionwords.dateFormatShouldBeDDMMYY();
    }

    @Then("established year format should be YY")
    public void establishedYearFormatShouldBeYY() {
        actionwords.establishedYearFormatShouldBeYY();
    }

    @Then("country of incorporation code should be displayed")
    public void countryOfIncorporationCodeShouldBeDisplayed(String freeText) {
        actionwords.countryOfIncorporationCodeShouldBeDisplayed(freeText);
    }

    @Then("CIF ID will be enterprise ID only")
    public void cIFIDWillBeEnterpriseIDOnly() {
        actionwords.cIFIDWillBeEnterpriseIDOnly();
    }

    @Then("customer status will always be active in this screen")
    public void customerStatusWillAlwaysBeActiveInThisScreen() {
        actionwords.customerStatusWillAlwaysBeActiveInThisScreen();
    }

    @Then("display enterprise customer details overview in list")
    public void displayEnterpriseCustomerDetailsOverviewInList() {
        actionwords.displayEnterpriseCustomerDetailsOverviewInList();
    }

    @When("check address information")
    public void checkAddressInformation() {
        actionwords.checkAddressInformation();
    }

    @Then("address type should be Home address or Mailing address or Office address or DEFAULT")
    public void addressTypeShouldBeHomeAddressOrMailingAddressOrOfficeAddressOrDEFAULT() {
        actionwords.addressTypeShouldBeHomeAddressOrMailingAddressOrOfficeAddressOrDEFAULT();
    }

    @Then("if there are more than one address, all addresses should be displayed")
    public void ifThereAreMoreThanOneAddressAllAddressesShouldBeDisplayed() {
        actionwords.ifThereAreMoreThanOneAddressAllAddressesShouldBeDisplayed();
    }

    @Then("the result table should be hidden")
    public void theResultTableShouldBeHidden() {
        actionwords.theResultTableShouldBeHidden();
    }

    @When("verify customer level authorization")
    public void verifyCustomerLevelAuthorization() {
        actionwords.verifyCustomerLevelAuthorization();
    }

    @Then("all the information in customer level authorization display correctly")
    public void allTheInformationInCustomerLevelAuthorizationDisplayCorrectly(String freeText) {
        actionwords.allTheInformationInCustomerLevelAuthorizationDisplayCorrectly(freeText);
    }

    @When("verify account level authorization")
    public void verifyAccountLevelAuthorization() {
        actionwords.verifyAccountLevelAuthorization();
    }

    @Then("all the information in account level authorization display correctly")
    public void allTheInformationInAccountLevelAuthorizationDisplayCorrectly(String freeText) {
        actionwords.allTheInformationInAccountLevelAuthorizationDisplayCorrectly(freeText);
    }

    @Then("verify the drop down list")
    public void verifyTheDropDownList(String freeText) {
        actionwords.verifyTheDropDownList(freeText);
    }

    @Then("display drop down list")
    public void displayDropDownList(String freeText) {
        actionwords.displayDropDownList(freeText);
    }

    @When("verify the displayed information")
    public void verifyTheDisplayedInformation() {
        actionwords.verifyTheDisplayedInformation();
    }

    @When("scan the barcode")
    public void scanTheBarcode() {
        actionwords.scanTheBarcode();
    }

    @When("enter device name {string}")
    public void enterDeviceNameDevicename(String devicename) {
        actionwords.enterDeviceNameDevicename(devicename);
    }

    @When("enter OTP {string} from authenticator")
    public void enterOTPOtpFromAuthenticator(String otp) {
        actionwords.enterOTPOtpFromAuthenticator(otp);
    }

    @When("enter OTP {string} from sms")
    public void enterOTPOtpFromSms(String otp) {
        actionwords.enterOTPOtpFromSms(otp);
    }

    @Given("customer's phone number has already marked as User For Contact")
    public void customersPhoneNumberHasAlreadyMarkedAsUserForContact() {
        actionwords.customersPhoneNumberHasAlreadyMarkedAsUserForContact();
    }

    @Given("customer's email has already marked as User For Contact")
    public void customersEmailHasAlreadyMarkedAsUserForContact() {
        actionwords.customersEmailHasAlreadyMarkedAsUserForContact();
    }

    @When("enter OTP {string} from email")
    public void enterOTPOtpFromEmail(String otp) {
        actionwords.enterOTPOtpFromEmail(otp);
    }

    @Then("user should be able to receive the email")
    public void userShouldBeAbleToReceiveTheEmail(String freeText) {
        actionwords.userShouldBeAbleToReceiveTheEmail(freeText);
    }

    @When("enter Username {string}")
    public void enterUsernameP1(String p1) {
        actionwords.enterUsernameP1(p1);
    }

    @When("enter National ID {string}")
    public void enterNationalIDP1(String p1) {
        actionwords.enterNationalIDP1(p1);
    }

    @When("enter mobile number {string}")
    public void enterMobileNumberP1(String p1) {
        actionwords.enterMobileNumberP1(p1);
    }
}