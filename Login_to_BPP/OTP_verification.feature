Feature: OTP verification


  @positive
  Scenario Outline: First time login by 2FA authentication
    Given navigate the "internet banking portal" url
    When the user enter user name "<username>" and password "<password>"
    And click the "login button"
    Then user able to see "the 2FA authentication screen"
      """
      barcode
      one-time code
      device name
      """
    When scan the barcode
    And enter OTP "<otp>" from authenticator
    And enter device name "<devicename>"
    And click the "submit button"
    Then user able to see "the dashboard"

    Examples:
      | username | password | otp | devicename |
      | Y090123 | YOMA123!@# | 123456 | thin |

  @positive
  Scenario Outline: Login by 2FA OTP verification
    Given navigate the "internet banking portal" url
    When the user enter user name "<username>" and password "<password>"
    And click the "login button"
    Then user able to see "the OTP screen"
    When enter OTP "<otp>" from authenticator
    And click the "submit button"
    Then user able to see "the dashboard"

    Examples:
      | username | password | otp |
      | Y090123 | YOMA123!@# | 123456 |

  @positive
  Scenario Outline: Login by SMS OTP verification
    Given navigate the "internet banking portal" url
    And customer's phone number has already marked as User For Contact
    When the user enter user name "<username>" and password "<password>"
    And click the "login button"
    Then user able to see "the OTP screen"
    When enter OTP "<otp>" from sms
    And click the "submit button"
    Then user able to see "the dashboard"

    Examples:
      | username | password | otp | devicename |
      | Y090123 | YOMA123!@# | 123456 | thin |

  @positive
  Scenario Outline: Login by email OTP verification
    Given navigate the "internet banking portal" url
    And customer's email has already marked as User For Contact
    When the user enter user name "<username>" and password "<password>"
    And click the "login button"
    Then user able to see "the OTP screen"
    And user should be able to receive the email
      """
      Dear Yoma Bank Customer,

      Please use the following One Time Password (OTP) to access the business banking portal. 
      Your OTP code is 873274. This OTP will expire in 3 minutes. Do not share this OTP with anyone.

      Thank you!
      Yoma Bank
      """
    When enter OTP "<otp>" from email
    And click the "submit button"
    Then user able to see "the dashboard"

    Examples:
      | username | password | otp | devicename |
      | Y090123 | YOMA123!@# | 123456 | thin |
