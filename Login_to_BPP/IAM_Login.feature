Feature: IAM Login
    "**identify access management**"

  @positive
  Scenario Outline: IAM login using valid credentials
    Given navigate the "internet banking portal" url
    When the user enter user name "<username>" and password "<password>"
    And click the "login button"
    Then user able to see "the OTP screen"

    Examples:
      | username | password |
      | Y090123 | YOMA123!@# |

  @negative
  Scenario Outline: IAM login using invalid username
    Given navigate the "internet baking portal" url
    When enter invalid user name "<invalidusr>" and password "<password>"
    And click the "login button"
    Then able to see error message for invalid username

    Examples:
      | invalidusr | password |
      | Y123123 | YOMA123!@# |

  @negative
  Scenario Outline: IAM login using invalid password
    Given navigate the "internet baking portal" url
    When the user enter user name "<username>" and invalid password "<invalidpassword>"
    And click the "login button"
    Then able to see error message for invalid password

    Examples:
      | username | invalidpassword |
      | Y009860 | 121212 |

  @negative
  Scenario Outline: IAM Account lock after invalid login attempt reach maximum limit
    * navigate the "internet banking portal" url
    * enter invalid user name "<username>" and/or invalid password "<password>" for the first time
    * click the "login button"
    * able to see error message for invalid credentials
    * enter invalid user name "<username>" and/or invalid password "<password>" for the second time
    * click the "login button"
    * able to see error message for invalid credentials
    * enter invalid user name "<username>" and/or invalid password "<password>" for the third time
    * click the "login button"
    * able to see error message for invalid credentials
    * enter invalid user name "<username>" and/or invalid password "<password>" for the fourth time
    * click the "login button"
    * able to see error message for invalid credentials
    * enter invalid user name "<username>" and/or invalid password "<password>" for the fifth time
    * click the "login button"
    * able to see error message for invalid credentials
    * enter invalid user name "<username>" and/or invalid password "<password>" for the sixth time
    * click the "login button"
    * able to see error message for invalid credentials

    Examples:
      | username | password |
      | Y009860 | YOMA123!@$%%^ |

  @negative
  Scenario Outline: IAM Blocked account login
    "**The user account is blocked from the FBE and user trying to login the blocked account**"
    Given navigate the "internet banking portal" url
    When the user enter user name "<username>" and password "<password>"
    And click the "login button"
    Then able to see error message (Something’s wrong with your account. Please contact our customer care center.)

    Examples:
      | username | password |
      | Y009860 | YOMA123!@# |

  @negative
  Scenario Outline: IAM CBM blacklisted user login
    "**user is blacklisted in CBM, yoma block/locked/suspended the account from FBE**"
    * navigate the "internet banking portal" url
    * the user enter user name "<username>" and password "<password>"
    * click the "login button"
    * able to see error message (Something’s wrong with your account. Please contact our customer care center.)

    Examples:
      | username | password |
      | username | password |
