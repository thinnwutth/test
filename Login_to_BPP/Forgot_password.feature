Feature: Forgot password


  @positive
  Scenario Outline: Reset forgot password using valid data
    Given navigate the "internet banking portal" url
    When click the "forgot password link from login page"
    Then user able to see "the forgot password screen"
      """
      Username
      National ID
      Mobile Number
      Button1: Back to Login
      Button2: Next
      """
    When enter Username "<username>"
    And enter National ID "<nationalID>"
    And enter mobile number "<mobilenumber>"
    And click the "Next button"
    Then user able to see "the successful message"

    Examples:
      | username | nationalID | mobilenumber |
      | bbpuser | 135555 | 09786712534 |

  @positive
  Scenario Outline: Reset forgot password using invalid data
    Given navigate the "internet banking portal" url
    When click the "forgot password link from login page"
    Then user able to see "the forgot password screen"
      """
      Username
      National ID
      Mobile Number
      Button1: Back to Login
      Button2: Next
      """
    When enter Username "<username>"
    And enter National ID "<nationalID>"
    And enter mobile number "<mobilenumber>"
    And click the "Next button"
    Then user able to see "the error message"

    Examples:
      | username | nationalID | mobilenumber |
      | bbpusersssss | 135555 | 09786712534 |
      | bbpuser | 0000 | 09786712534 |
      | bbpuser | 135555 | abc |
